﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace SearchWF
{
    public partial class Form1 : Form
    {

        private static string startDirectory, namePattern, textInsideFile;
        private static bool frst = true, ended = false;

        public Form1()
        {
            InitializeComponent();
            if ((File.Exists("data.txt")))
            {
                List<string> lines = File.ReadLines("data.txt").ToList();
                if (lines.Count() >= 3)
                {
                    startDirectoryTextBox.Text = startDirectory = lines[0];
                    namePatternTextBox.Text = lines[1];
                    textInsideFileTextBox.Text = lines[2];
                }
            }
            else
            {
                string[] str = { "", "", "" };
                File.WriteAllLines("data.txt", str);
            }
        }

        private void startDirectoryButton_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                startDirectoryTextBox.Text = startDirectory = folderBrowserDialog.SelectedPath.ToString();
                namePattern = namePatternTextBox.Text;
                textInsideFile = textInsideFileTextBox.Text;
                string[] str = { startDirectory, namePattern, textInsideFile };
                File.WriteAllLines("data.txt", str);
                direcoryLenght = startDirectory.Split('\\').Length;
            }

        }

        //Вывод древа файлов

        private int stringsDispayed, updated;
        private List<TreeNode> nodeList = new List<TreeNode>();

        private void updateTreeView()
        {
            var nodeArray = nodeList.ToArray();
            //treeViewFiles.Nodes.Clear();
            foreach (TreeNode treeNode in nodeArray)
            {
                if (!treeViewFiles.Nodes.Contains(treeNode))
                {
                    treeViewFiles.Nodes.Add(treeNode);
                    //treeViewFiles.Nodes.
                }
            }
            //treeViewFiles.Nodes.AddRange(nodeArray);
            updated = stringsDispayed;
        }

        private void createNodeList()
        {
            if (searchedFiles != null)
            {
                while (stringsDispayed < searchedFiles.Count)
                {
                    nodeList.Add(createNodesFromString(searchedFiles[stringsDispayed]));
                    System.Diagnostics.Debug.WriteLine(searchedFiles[stringsDispayed]);
                    stringsDispayed++;
                }
            }
        }

        private int direcoryLenght;
        private TreeNode createNodesFromString(string inputString)
        {
            string[] names = inputString.Split('\\');

            TreeNode outputTreeNode = new TreeNode(names[names.Length - 1]);
            for (int i = names.Length - 2; i > direcoryLenght; i--)
            {
                //treeViewFiles.Nodes.ContainsKey
                outputTreeNode = createNodes(outputTreeNode, ref names[i]);
            }

            return outputTreeNode;
        }

        private TreeNode createNodes(TreeNode inputTreeNode, ref string inputString)
        {
            TreeNode outputTreeNode = new TreeNode(inputString);
            outputTreeNode.Nodes.Add(inputTreeNode);
            return outputTreeNode;
        }

        private Thread threadGlobal;
        private void startSearchButton_Click(object sender, EventArgs e)
        {
            if (stringsNotEmpty())
            {
                timerLabel.Text = simpleTimer.Reset();
                timer.Enabled = true;
                filesProcessedCounter = 0;
                processingNow = "";
                buttonsEnabler(false, true, false);
                if (!frst)
                {
                    searchedFiles = new List<string>();
                    if (threadGlobal.IsAlive)
                    {
                        threadGlobal.Resume();
                        threadGlobal.Abort();
                    }
                }
                frst = false;
                threadGlobal = new Thread(() => searchStarted(startDirectory));
                threadGlobal.IsBackground = true;
                threadGlobal.Priority = ThreadPriority.Highest;
                threadGlobal.Start();
            }
            else
                MessageBox.Show("Заполните все поля!", "Ошибка");
        }

        private void stopSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                threadGlobal.Suspend();
                buttonsEnabler(true, false, true);
                timer.Enabled = false;
                //sleep = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка");
            }
        }

        private void resumeSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                threadGlobal.Resume();
                buttonsEnabler(false, true, false);
                timer.Enabled = true;
                //sleep = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка");
            }
        }

        SimpleTimer simpleTimer = new SimpleTimer();
        private void timer_Tick(object sender, EventArgs e)
        {
            timerLabel.Text = simpleTimer.Tick();
        }

        private void updateTimer_Tick(object sender, EventArgs e)
        {
            filesProcessedLabel.Text = filesProcessedCounter.ToString();
            ProcessingFileTextBox.Text = processingNow;
            if (ended)
            {
                buttonsEnabler(true, false, false);
                timer.Enabled = false;
                ended = false;
            }
            createNodeList();
            if (stringsDispayed > updated)
            {
                updateTreeView();
            }
        }

        private void namePatternTextBox_TextChanged(object sender, EventArgs e)
        {
            namePattern = namePatternTextBox.Text;
            textInsideFile = textInsideFileTextBox.Text;
            string[] str = { startDirectory, namePattern, textInsideFile };
            File.WriteAllLines("data.txt", str);
        }

        private void textInsideFileTextBox_TextChanged(object sender, EventArgs e)
        {
            textInsideFile = textInsideFileTextBox.Text;
            namePattern = namePatternTextBox.Text;
            string[] str = { startDirectory, namePattern, textInsideFile };
            File.WriteAllLines("data.txt", str);
        }

        private bool stringsNotEmpty()
        {
            return (startDirectory != null) && (startDirectory != "")
                   && (namePattern != null) && (namePattern != "");
        }

        private void buttonsEnabler(bool startSearchButtonEnabled, bool stopSearchButtonEnabled, bool resumeSearchButtonEnabled)
        {
            startSearchButton.Enabled = startSearchButtonEnabled;
            stopSearchButton.Enabled = stopSearchButtonEnabled;
            resumeSearchButton.Enabled = resumeSearchButtonEnabled;
        }

        private static int filesProcessedCounter = 0;
        private static string processingNow = "";
        private static List<string> searchedFiles = new List<string>();

        private static void recursiveSearch(string inputStartDirectory)
        {
            try
            {
                string[] filesCollection = Directory.GetFiles(inputStartDirectory, namePattern);
                filesProcessedCounter += (Directory.GetFiles(inputStartDirectory).Length - filesCollection.Length);
                foreach (string st in filesCollection)
                {
                    string text = File.ReadAllText(st);
                    processingNow = st;
                    System.Diagnostics.Debug.WriteLine(st);
                    if (text.Contains(textInsideFile))
                    {
                        searchedFiles.Add(st);
                    }
                    filesProcessedCounter++;
                }
                string[] directoryCollection = Directory.GetDirectories(inputStartDirectory);
                foreach (string st in directoryCollection)
                {
                    recursiveSearch(st);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Ошибка:" + ex);
                //MessageBox.Show("Ошибка:" + ex.Message);
            }
        }

        private static void searchStarted(string startDirectory)
        {
            System.Diagnostics.Debug.WriteLine("Запущен task");
            recursiveSearch(startDirectory);
            System.Diagnostics.Debug.WriteLine("Поиск завершен");
            MessageBox.Show("Поиск завершен, найдено: " + searchedFiles.Count + " файла(ов).", "Конец");
            ended = true;
        }
    }
}
