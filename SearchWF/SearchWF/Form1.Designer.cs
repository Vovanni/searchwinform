﻿namespace SearchWF
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.namePatternTextBox = new System.Windows.Forms.TextBox();
            this.namePatternLabel = new System.Windows.Forms.Label();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.startDirectoryLabel = new System.Windows.Forms.Label();
            this.startDirectoryButton = new System.Windows.Forms.Button();
            this.startDirectoryTextBox = new System.Windows.Forms.RichTextBox();
            this.textInsideFileTextBox = new System.Windows.Forms.RichTextBox();
            this.textInsideFileLabel = new System.Windows.Forms.Label();
            this.startSearchButton = new System.Windows.Forms.Button();
            this.stopSearchButton = new System.Windows.Forms.Button();
            this.resumeSearchButton = new System.Windows.Forms.Button();
            this.treeViewFiles = new System.Windows.Forms.TreeView();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.timerLabel = new System.Windows.Forms.Label();
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.filesProcessedLabel = new System.Windows.Forms.Label();
            this.TimerNameLabel = new System.Windows.Forms.Label();
            this.NumberNameLabel = new System.Windows.Forms.Label();
            this.processingFileNameLabel = new System.Windows.Forms.Label();
            this.ProcessingFileTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // namePatternTextBox
            // 
            this.namePatternTextBox.Location = new System.Drawing.Point(277, 121);
            this.namePatternTextBox.Name = "namePatternTextBox";
            this.namePatternTextBox.Size = new System.Drawing.Size(652, 22);
            this.namePatternTextBox.TabIndex = 0;
            this.namePatternTextBox.TextChanged += new System.EventHandler(this.namePatternTextBox_TextChanged);
            // 
            // namePatternLabel
            // 
            this.namePatternLabel.Location = new System.Drawing.Point(277, 82);
            this.namePatternLabel.Name = "namePatternLabel";
            this.namePatternLabel.Size = new System.Drawing.Size(652, 36);
            this.namePatternLabel.TabIndex = 1;
            this.namePatternLabel.Text = "Шаблон имени файла . Этот параметр может содержать сочетание допустимого литераль" +
    "ного пути и подстановочного символа (* и ?).";
            // 
            // startDirectoryLabel
            // 
            this.startDirectoryLabel.AutoSize = true;
            this.startDirectoryLabel.Location = new System.Drawing.Point(274, 6);
            this.startDirectoryLabel.Name = "startDirectoryLabel";
            this.startDirectoryLabel.Size = new System.Drawing.Size(205, 17);
            this.startDirectoryLabel.TabIndex = 3;
            this.startDirectoryLabel.Text = "Выбор стартовой директории";
            // 
            // startDirectoryButton
            // 
            this.startDirectoryButton.Location = new System.Drawing.Point(277, 26);
            this.startDirectoryButton.Name = "startDirectoryButton";
            this.startDirectoryButton.Size = new System.Drawing.Size(147, 44);
            this.startDirectoryButton.TabIndex = 4;
            this.startDirectoryButton.Text = "Выбрать стартовую директорию";
            this.startDirectoryButton.UseVisualStyleBackColor = true;
            this.startDirectoryButton.Click += new System.EventHandler(this.startDirectoryButton_Click);
            // 
            // startDirectoryTextBox
            // 
            this.startDirectoryTextBox.Location = new System.Drawing.Point(431, 26);
            this.startDirectoryTextBox.Name = "startDirectoryTextBox";
            this.startDirectoryTextBox.ReadOnly = true;
            this.startDirectoryTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.startDirectoryTextBox.Size = new System.Drawing.Size(498, 43);
            this.startDirectoryTextBox.TabIndex = 5;
            this.startDirectoryTextBox.Text = "";
            // 
            // textInsideFileTextBox
            // 
            this.textInsideFileTextBox.Location = new System.Drawing.Point(277, 177);
            this.textInsideFileTextBox.Name = "textInsideFileTextBox";
            this.textInsideFileTextBox.Size = new System.Drawing.Size(652, 122);
            this.textInsideFileTextBox.TabIndex = 6;
            this.textInsideFileTextBox.Text = "";
            this.textInsideFileTextBox.TextChanged += new System.EventHandler(this.textInsideFileTextBox_TextChanged);
            // 
            // textInsideFileLabel
            // 
            this.textInsideFileLabel.AutoSize = true;
            this.textInsideFileLabel.Location = new System.Drawing.Point(274, 157);
            this.textInsideFileLabel.Name = "textInsideFileLabel";
            this.textInsideFileLabel.Size = new System.Drawing.Size(206, 17);
            this.textInsideFileLabel.TabIndex = 7;
            this.textInsideFileLabel.Text = "Текст содержащийся в файле";
            // 
            // startSearchButton
            // 
            this.startSearchButton.Location = new System.Drawing.Point(277, 414);
            this.startSearchButton.Name = "startSearchButton";
            this.startSearchButton.Size = new System.Drawing.Size(212, 43);
            this.startSearchButton.TabIndex = 8;
            this.startSearchButton.Text = "Начать новый поиск";
            this.startSearchButton.UseVisualStyleBackColor = true;
            this.startSearchButton.Click += new System.EventHandler(this.startSearchButton_Click);
            // 
            // stopSearchButton
            // 
            this.stopSearchButton.Location = new System.Drawing.Point(499, 414);
            this.stopSearchButton.Name = "stopSearchButton";
            this.stopSearchButton.Size = new System.Drawing.Size(212, 43);
            this.stopSearchButton.TabIndex = 9;
            this.stopSearchButton.Text = "Остановить поиск";
            this.stopSearchButton.UseVisualStyleBackColor = true;
            this.stopSearchButton.Click += new System.EventHandler(this.stopSearchButton_Click);
            // 
            // resumeSearchButton
            // 
            this.resumeSearchButton.Location = new System.Drawing.Point(717, 414);
            this.resumeSearchButton.Name = "resumeSearchButton";
            this.resumeSearchButton.Size = new System.Drawing.Size(212, 43);
            this.resumeSearchButton.TabIndex = 10;
            this.resumeSearchButton.Text = "Продолжить поиск";
            this.resumeSearchButton.UseVisualStyleBackColor = true;
            this.resumeSearchButton.Click += new System.EventHandler(this.resumeSearchButton_Click);
            // 
            // treeViewFiles
            // 
            this.treeViewFiles.Location = new System.Drawing.Point(12, 6);
            this.treeViewFiles.Name = "treeViewFiles";
            this.treeViewFiles.Size = new System.Drawing.Size(256, 451);
            this.treeViewFiles.TabIndex = 11;
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // timerLabel
            // 
            this.timerLabel.AutoSize = true;
            this.timerLabel.Location = new System.Drawing.Point(345, 304);
            this.timerLabel.Name = "timerLabel";
            this.timerLabel.Size = new System.Drawing.Size(64, 17);
            this.timerLabel.TabIndex = 12;
            this.timerLabel.Text = "00:00:00";
            // 
            // updateTimer
            // 
            this.updateTimer.Enabled = true;
            this.updateTimer.Interval = 50;
            this.updateTimer.Tick += new System.EventHandler(this.updateTimer_Tick);
            // 
            // filesProcessedLabel
            // 
            this.filesProcessedLabel.AutoSize = true;
            this.filesProcessedLabel.Location = new System.Drawing.Point(596, 304);
            this.filesProcessedLabel.Name = "filesProcessedLabel";
            this.filesProcessedLabel.Size = new System.Drawing.Size(16, 17);
            this.filesProcessedLabel.TabIndex = 13;
            this.filesProcessedLabel.Text = "0";
            // 
            // TimerNameLabel
            // 
            this.TimerNameLabel.AutoSize = true;
            this.TimerNameLabel.Location = new System.Drawing.Point(274, 304);
            this.TimerNameLabel.Name = "TimerNameLabel";
            this.TimerNameLabel.Size = new System.Drawing.Size(65, 17);
            this.TimerNameLabel.TabIndex = 15;
            this.TimerNameLabel.Text = "Прошло:";
            // 
            // NumberNameLabel
            // 
            this.NumberNameLabel.AutoSize = true;
            this.NumberNameLabel.Location = new System.Drawing.Point(496, 304);
            this.NumberNameLabel.Name = "NumberNameLabel";
            this.NumberNameLabel.Size = new System.Drawing.Size(94, 17);
            this.NumberNameLabel.TabIndex = 16;
            this.NumberNameLabel.Text = "Обработано:";
            // 
            // processingFileNameLabel
            // 
            this.processingFileNameLabel.AutoSize = true;
            this.processingFileNameLabel.Location = new System.Drawing.Point(274, 321);
            this.processingFileNameLabel.Name = "processingFileNameLabel";
            this.processingFileNameLabel.Size = new System.Drawing.Size(174, 17);
            this.processingFileNameLabel.TabIndex = 17;
            this.processingFileNameLabel.Text = "Сейчас обрабатывается:";
            // 
            // ProcessingFileTextBox
            // 
            this.ProcessingFileTextBox.Location = new System.Drawing.Point(277, 341);
            this.ProcessingFileTextBox.Name = "ProcessingFileTextBox";
            this.ProcessingFileTextBox.ReadOnly = true;
            this.ProcessingFileTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.ProcessingFileTextBox.Size = new System.Drawing.Size(652, 67);
            this.ProcessingFileTextBox.TabIndex = 18;
            this.ProcessingFileTextBox.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 466);
            this.Controls.Add(this.ProcessingFileTextBox);
            this.Controls.Add(this.processingFileNameLabel);
            this.Controls.Add(this.NumberNameLabel);
            this.Controls.Add(this.TimerNameLabel);
            this.Controls.Add(this.filesProcessedLabel);
            this.Controls.Add(this.timerLabel);
            this.Controls.Add(this.treeViewFiles);
            this.Controls.Add(this.resumeSearchButton);
            this.Controls.Add(this.stopSearchButton);
            this.Controls.Add(this.startSearchButton);
            this.Controls.Add(this.textInsideFileLabel);
            this.Controls.Add(this.textInsideFileTextBox);
            this.Controls.Add(this.startDirectoryTextBox);
            this.Controls.Add(this.startDirectoryButton);
            this.Controls.Add(this.startDirectoryLabel);
            this.Controls.Add(this.namePatternLabel);
            this.Controls.Add(this.namePatternTextBox);
            this.Name = "Form1";
            this.Text = "SearchWF";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox namePatternTextBox;
        private System.Windows.Forms.Label namePatternLabel;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Label startDirectoryLabel;
        private System.Windows.Forms.Button startDirectoryButton;
        private System.Windows.Forms.RichTextBox startDirectoryTextBox;
        private System.Windows.Forms.RichTextBox textInsideFileTextBox;
        private System.Windows.Forms.Label textInsideFileLabel;
        private System.Windows.Forms.Button startSearchButton;
        private System.Windows.Forms.Button stopSearchButton;
        private System.Windows.Forms.Button resumeSearchButton;
        private System.Windows.Forms.TreeView treeViewFiles;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label timerLabel;
        private System.Windows.Forms.Timer updateTimer;
        private System.Windows.Forms.Label filesProcessedLabel;
        private System.Windows.Forms.Label TimerNameLabel;
        private System.Windows.Forms.Label NumberNameLabel;
        private System.Windows.Forms.Label processingFileNameLabel;
        private System.Windows.Forms.RichTextBox ProcessingFileTextBox;
    }
}

