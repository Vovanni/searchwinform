﻿using System;

public class SimpleTimer
{
	public SimpleTimer()
	{
        timerClk = new time();
    }

    private struct time
    {
        public ushort seconds;
        public ushort minutes;
        public ushort hours;
    }

    private time timerClk;

    public string Reset()
    {
        timerClk = new time();
        return "00:00:00";
    }

    public string Tick ()
    {
        timerClk.seconds++;
        if (timerClk.seconds == 60)
        {
            timerClk.minutes++;
            timerClk.seconds = 0;
            if (timerClk.minutes == 60)
            {
                timerClk.hours++;
                timerClk.minutes = 0;
            }
        }
        return addNull(timerClk.hours) + ":" + addNull(timerClk.minutes) + ":" + addNull(timerClk.seconds);
    }

    private string addNull(ushort input)
    {
        if (input < 10) return "0" + input;
        return input.ToString();
    }
}
